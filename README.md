![ez logo](/resources/images/ez/ez-smiley-small-logo.png)
# EZ! Jenkins Kubernetes installation

The project creates HELM chart, and upload it to DockerHub. See:<br/> https://hub.docker.com/r/ezyorammi/ez-framework

The project also contains a GitLab-CI piprline that build versions of the HELM chart and upload it to DockerHub. 

